{ 
  description = "Berkeley Mono NerdFonts"; 
  inputs = { 
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable"; 
    # flake-utils.url = "github:numtide/flake-utils"; 
  }; 

  outputs = { self, nixpkgs, ... }@inputs: {
    berkeley-mono = nixpkgs.stdenv.mkDerivation {
      name = "berkeley-mono";
      version = "1.0";
      src = self;

      installPhase = ''
        mkdir -p $out/share/fonts/truetype/berkeley-mono
	cp -r $src/berkeley-mono/* $out/share/fonts/truetype/berkeley-mono
      '';
    };
  };
    

}
